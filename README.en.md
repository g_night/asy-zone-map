# asy-zone-map

#### Description
Partition read/write lock + Map: Maps with similar read/write ratios and high concurrent security are supported

Serves the: asy-cache project（[click here](https://gitee.com/g_night/asy-cache)）, which will later replace the original single read/write lock map

#### Installation

Refer to the test file for details

```go 
// Test whether the initialization is correct
func TestZoneMap_New(t *testing.T) {
	// 1. Set the expected range
	arr := []int{8, 16, 32, 64}
	// 2. Check for multiple inputs
	for i := -100; i < 200; i++ {
		m := NewWith(nil, 128)
		j := sort.SearchInts(arr, m.Size())
		if j >= m.Size() || arr[j] != m.Size() {
			t.Error("NewWith initialization rule not expected: 8 to 64")
		}
	}
}

// Test: Get & Set & Delete
func TestZoneMap_Get_Set_Delete(t *testing.T) {
	m := New()

	// 1. Test Get
	val, ok := m.Get("no")
	if ok {
		t.Error(" No should not exist, OK should return false")
	}
	if val != nil {
		t.Error("val should return nil")
	}

	// 2. Test Set: Set yes
	m.Set("yes", "yes")
	s, ok := m.Get("yes")
	if !ok {
		t.Error(" Yes should exist, OK should return true")
	}
	ts, ok := s.(string)
	if !ok {
		t.Error(" Yes should be successfully converted to string ")
	}
	if ts != "yes" {
		t.Error(" TS should be converted to yes")
	}

	// 3. Test Delete: Delete yes
	m.Delete("yes")
	// Should not exist
	_, ok = m.Get("yes")
	if ok {
		t.Error(" Yes has been deleted, OK should return false")
	}
}

// Test: GetOrSet
func TestZoneMap_GetOrSet(t *testing.T) {
	m := New()

	// 1. Try to set a value that does not exist before, it should exist after setting
	ok := m.GetOrSet("GetOrSet", "GetOrSet")
	if !ok {
		t.Error("GetOrSet should not exist, OK should return true")
	}

	// 2. Check whether the configuration is successful
	s, ok := m.Get("GetOrSet")
	if !ok {
		t.Error("GetOrSet should exist ")
	}

	// 3. Verify the correctness of the deposit
	ts, ok := s.(string)
	if !ok {
		t.Error("GetOrSet should be successfully converted to string ")
	}
	if ts != "GetOrSet" {
		t.Error(" TS should be converted to GetOrSet")
	}
}

// Test: MoreSet, check whether the batch can be stored
func TestZoneMap_MoreSet(t *testing.T) {
	m := New()
	// 1. Initialize the map to store
	data := make(map[string]interface{})
	for i := 0; i < 10; i++ {
		data[strconv.Itoa(i)] = i
	}
	// 2. Check whether the configuration is successful
	m.MoreSet(data)
	for i := 0; i < 10; i++ {
		_, ok := m.Get(strconv.Itoa(i))
		if !ok {
			t.Error("MoreSet should store this key = ", i)
		}
	}
}

// Test: Delete Custom Delete callback judgment -deletefunc
func TestZoneMap_DeleteFunc(t *testing.T) {
	m := New()

	// 1. Set DeleteCallBack to true if num % 2 == 0
	fc := func(key, value interface{}, exist bool) interface{} {
		if exist && value.(int)%2 == 0 {
			return true
		}
		return false
	}
	// insert num from 1 to 10
	for i := 1; i <= 10; i++ {
		m.Set(strconv.Itoa(i), i)
	}

	// 3. Delete all and call DeleteCallBack. The result should be calculated as: 5
	count := 0
	for i := 1; i <= 10; i++ {
		even := m.DeleteFunc(strconv.Itoa(i), fc)
		if even.(bool) {
			count++
		}
	}
	if count != 5 {
		t.Error("DeleteCallBack calculated callback error, should be 5")
	}
}

// Test: Range traverses the function to read the specified stop
func TestZoneMap_Range(t *testing.T) {
	m := New()

	// 1. Initialize the map to store
	for i := 1; i <= 10; i++ {
		m.Set(strconv.Itoa(i), i)
	}

	// 2. Stop at 3
	end := "-1"
	f := func(key string, value interface{}) bool {
		t.Log("key = ", key)
		if key == "3" {
			end = key
			return false
		}
		return true
	}

	// 3. Start range and stop when "3" is reached
	m.Range(f)
	if end != "3" {
		t.Error("Range end condition should be 3 ")
	}
}

```

