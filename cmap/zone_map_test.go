package cmap

import (
	"sort"
	"strconv"
	"testing"
)

// 测试：初始化是否正确
func TestZoneMap_New(t *testing.T) {
	// 1.设置预期范围
	arr := []int{8, 16, 32, 64}

	// 2.检查多个输入的情况
	for i := -100; i < 200; i++ {
		m := NewWith(nil, 128)
		j := sort.SearchInts(arr, m.Size())
		if j >= m.Size() || arr[j] != m.Size() {
			t.Error("NewWith 初始化规则不符合预期：8~64")
		}
	}
}

// 测试：Get & Set & Delete
func TestZoneMap_Get_Set_Delete(t *testing.T) {
	m := New()

	// 1.测试Get功能
	val, ok := m.Get("no")
	if ok {
		t.Error("no 不应该存在，ok 应该返回 false")
	}
	if val != nil {
		t.Error("val 应该返回nil")
	}

	// 2.测试Set：设置yes
	m.Set("yes", "yes")
	s, ok := m.Get("yes")
	if !ok {
		t.Error("yes 应该存在，ok 应该返回 true")
	}
	ts, ok := s.(string)
	if !ok {
		t.Error("yes 应该成功转换为 string 类型")
	}
	if ts != "yes" {
		t.Error("ts 应该转为 yes")
	}

	// 3.测试Delete：删除yes
	m.Delete("yes")
	// 不应该存在
	_, ok = m.Get("yes")
	if ok {
		t.Error("yes 已经被删除，ok 应该返回 false")
	}
}

// 测试：GetOrSet
func TestZoneMap_GetOrSet(t *testing.T) {
	m := New()

	// 1.尝试设置原先不存在的值，设置后应该存在
	ok := m.GetOrSet("GetOrSet", "GetOrSet")
	if !ok {
		t.Error("GetOrSet 原先不应该存在，ok 应该返回 true")
	}

	// 2.检查设置是否成功
	s, ok := m.Get("GetOrSet")
	if !ok {
		t.Error("GetOrSet 应该存在")
	}

	// 3.检验存入的正确性
	ts, ok := s.(string)
	if !ok {
		t.Error("GetOrSet 应该成功转换为 string 类型")
	}
	if ts != "GetOrSet" {
		t.Error("ts 应该转为 GetOrSet")
	}
}

// 测试：MoreSet，检验是否可以批量存入
func TestZoneMap_MoreSet(t *testing.T) {
	m := New()

	// 1.初始化要存入的map
	data := make(map[string]interface{})
	for i := 0; i < 10; i++ {
		data[strconv.Itoa(i)] = i
	}

	// 2.检查设置是否成功
	m.MoreSet(data)
	for i := 0; i < 10; i++ {
		_, ok := m.Get(strconv.Itoa(i))
		if !ok {
			t.Error("MoreSet 应该存入该 key = ", i)
		}
	}
}

// 测试：Delete自定义删除后的回调判断 - DeleteFunc
func TestZoneMap_DeleteFunc(t *testing.T) {
	m := New()

	// 1.设置DeleteCallBack，满足 num % 2 == 0 的时候返回 true
	fc := func(key, value interface{}, exist bool) interface{} {
		if exist && value.(int)%2 == 0 {
			return true
		}
		return false
	}

	// 2.存入1~10，偶数有5个
	for i := 1; i <= 10; i++ {
		m.Set(strconv.Itoa(i), i)
	}

	// 3.删除全部并调用DeleteCallBack，结果应该计算出：5
	count := 0
	for i := 1; i <= 10; i++ {
		even := m.DeleteFunc(strconv.Itoa(i), fc)
		if even.(bool) {
			count++
		}
	}
	if count != 5 {
		t.Error("DeleteCallBack计算回调有误，应该为5")
	}
}

// 测试：Range遍历函数，读取到指定的停止
func TestZoneMap_Range(t *testing.T) {
	m := New()

	// 1.初始化要存入的map
	for i := 1; i <= 10; i++ {
		m.Set(strconv.Itoa(i), i)
	}

	// 2.读取到3的时候停止
	end := "-1"
	f := func(key string, value interface{}) bool {
		t.Log("key = ", key)
		if key == "3" {
			end = key
			return false
		}
		return true
	}

	// 3.开始运行range，最后遇到 ”3“ 就停止
	m.Range(f)
	if end != "3" {
		t.Error("Range 结束条件应该为 3 ")
	}
}
