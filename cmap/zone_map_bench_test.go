package cmap

import (
	"strconv"
	"sync"
	"testing"
)

// 测试zone-map ：
// BenchmarkItems-8   	       1	1170953500 ns/op
// BenchmarkItems-8   	       1	1048417800 ns/op
func BenchmarkItems(b *testing.B) {
	m := New()

	b.ResetTimer()
	wg := sync.WaitGroup{}
	for i := 0; i < 15; i++ {
		wg.Add(1)
		if i < 5 {
			go func() {
				defer wg.Done()
				for n := 0; n < b.N; n++ {
					for i := 0; i < 1000000; i++ {
						m.Set(strconv.Itoa(i), i)
					}
				}
			}()
		} else {
			go func() {
				defer wg.Done()
				for n := 0; n < b.N; n++ {
					for i := 0; i < 1000000; i++ {
						m.Get(strconv.Itoa(i))
					}
				}
			}()
		}
	}
	wg.Wait()
}

// 测试sync.map ：
// BenchmarkSyncMapItems-8   	       1	4327257700 ns/op
// BenchmarkSyncMapItems-8   	       1	4234923100 ns/op
func BenchmarkSyncMapItems(b *testing.B) {
	m := sync.Map{}
	b.ResetTimer()
	wg := sync.WaitGroup{}
	for i := 0; i < 15; i++ {
		wg.Add(1)
		if i < 5 {
			go func() {
				defer wg.Done()
				for n := 0; n < b.N; n++ {
					for i := 0; i < 1000000; i++ {
						m.Store(strconv.Itoa(i), i)
					}
				}
			}()
		} else {
			go func() {
				defer wg.Done()
				for n := 0; n < b.N; n++ {
					for i := 0; i < 1000000; i++ {
						m.Load(strconv.Itoa(i))
					}
				}
			}()
		}
	}
	wg.Wait()
}
